package com.dao;

import java.util.List;
import java.util.Optional;

import com.model.Car;

public interface CarDao {
	public void addCar(Car car);

	public void updateCar(Car product);

	public void deleteCar(int id);
	
	public Optional<Car> findById(int id);
	
	public int findLastestId();
	
	public List<Car> findAll();
	
	public void exportCars(List<Car> cars);
}
