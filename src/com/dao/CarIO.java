package com.dao;

import java.io.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.model.Brand;
import com.model.Car;
import com.model.Color;
import com.service.DateFormat;

public class CarIO implements CarDao {
	private static final String FILE_PATH = "car.txt";
	private File file;

	public CarIO() {
		file = new File(FILE_PATH);
		createFile(file);
	}

	@Override
	public void addCar(Car car) {
		try {
			PrintWriter out = new PrintWriter(new FileWriter(file, true));
			String data = car.getId() + "|" + car.getName() + "|" + car.getBrand() + "|" + car.getPrice() + "|"
					+ car.getCreateDate();
			out.println(data);
			out.close();
		} catch (Exception e) {
			System.out.println("Error in add car : " + e);
		}
	}

	@Override
	public void updateCar(Car car) {
		try {
			BufferedReader in = new BufferedReader(new FileReader(file));
			List<String> fileContent = new ArrayList<>();

			String line;
			while ((line = in.readLine()) != null) {
				String data[] = line.split("\\|");
				if (data[0].equals(String.valueOf(car.getId()))) {
					String newData = car.getId() + "|" + car.getName() + "|" + car.getBrand() + "|" + car.getPrice()
							+ "|" + car.getCreateDate();
					fileContent.add(newData);
				} else {
					fileContent.add(line);
				}
			}
			in.close();

			PrintWriter out = new PrintWriter(new FileWriter(file, false));
			for (String carData : fileContent) {
				out.println(carData);
			}
			out.close();
		} catch (Exception e) {
			System.out.println("Error in update car : " + e);
		}
	}

	@Override
	public void deleteCar(int id) {
		try {
			BufferedReader in = new BufferedReader(new FileReader(file));
			List<String> fileContent = new ArrayList<>();

			String line;
			while ((line = in.readLine()) != null) {
				String data[] = line.split("\\|");
				if (!data[0].equals(String.valueOf(id))) {
					fileContent.add(line);
				}
			}
			in.close();

			PrintWriter out = new PrintWriter(new FileWriter(file, false));
			for (String carData : fileContent) {
				out.println(carData);
			}
			out.close();
		} catch (Exception e) {
			System.out.println("Error in delete car : " + e);
		}
	}

	@Override
	public List<Car> findAll() {
		List<Car> cars = new ArrayList<Car>();
		try {
			BufferedReader in = new BufferedReader(new FileReader(file));

			String line;
			while ((line = in.readLine()) != null) {
				String[] data = line.split("\\|");
				int id = Integer.parseInt(data[0]);
				String name = data[1];
				String brand = data[2];
				double price = Double.parseDouble(data[3]);
				String createDate = data[4];
				Car car = new Car(id, Brand.valueOf(brand), price, name, createDate);
				cars.add(car);
			}
			in.close();
		} catch (Exception e) {
			System.out.println("Error in findall : " + e);
		}
		return cars;
	}

	@Override
	public Optional<Car> findById(int id) {
		try {
			BufferedReader in = new BufferedReader(new FileReader(file));
			String line;
			while ((line = in.readLine()) != null) {
				String data[] = line.split("\\|");
				if (data[0].equals(String.valueOf(id))) {
					id = Integer.parseInt(data[0]);
					String name = data[1];
					String brand = data[2];
					double price = Double.parseDouble(data[3]);
					String createDate = data[4];
					Car car = new Car(id, Brand.valueOf(brand), price, name, createDate);
					in.close();
					return Optional.ofNullable(car);
				}
			}
			in.close();
		} catch (Exception e) {
			System.out.println("Error in find one car : " + e);
		}
		return Optional.empty();
	}

	@Override
	public int findLastestId() {
		try {
			BufferedReader in = new BufferedReader(new FileReader(file));
			String line, last = "";
			while ((line = in.readLine()) != null) {
				last = line;
			}
			String data[] = last.split("\\|");
			int id = Integer.parseInt(data[0]);
			in.close();
			return id;
		} catch (Exception e) {
			System.out.println("Error in find one car : " + e);
		}
		return 0;
	}

	@Override
	public void exportCars(List<Car> cars) {
		FileOutputStream fos = null;
		ObjectOutputStream oos = null;
		System.out.println(cars);
		File f = new File((DateFormat.formatDateReport(LocalDateTime.now())+".txt"));
		createFile(f);
		try {
			fos = new FileOutputStream(f);
			oos = new ObjectOutputStream(fos);
			oos.writeObject(cars);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			closeStream(fos);
			closeStream(oos);
		}
	}

	private void closeStream(OutputStream os) {
		if (os != null) {
			try {
				os.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	private void createFile(File f) {
		try {
			if (!f.exists()) {
				f.createNewFile();
			} else {
				System.out.println("File already exists");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
