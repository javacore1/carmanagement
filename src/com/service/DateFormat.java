package com.service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class DateFormat {
	public static String formatDate(LocalDateTime date) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		return date.format(formatter);
	}
	public static String formatDateReport(LocalDateTime date) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy_HH-mm-ss");
		return date.format(formatter);
	}
}
