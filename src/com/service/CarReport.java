package com.service;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Writer;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import com.dao.CarDao;
import com.model.Car;

public class CarReport implements Runnable {

	private CarDao carDao;

	public CarReport(CarDao carDao) {
		this.carDao = carDao;
	}

	@Override
	public void run() {
		while (true) {
			try {
				// run the operation every 2 minutes
				Thread.sleep(2 * 60 * 1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			List<Car> cars = carDao.findAll().stream()
	    			.sorted(Comparator.comparing(Car::getPrice).reversed())
	    			.limit(5)
	    			.collect(Collectors.toList());
			carDao.exportCars(cars);
		}
		
	}

//	public void writeContent(Writer writer) {
//		carDao.findAll().stream().sorted(Comparator.comparing(Car::getPrice).reversed()).limit(5)
//				.map(o -> o.getId() + " --- " + o.getName() + " --- " + o.getBrand() + " --- " + o.getPrice() + "$ --- " + o.getCreateDate()+"\r\n")
//				.forEach(t -> {
//					try {
//						writer.write(t);
//					} catch (IOException e) {
//						e.printStackTrace();
//					}		
//				});
//	}
}
