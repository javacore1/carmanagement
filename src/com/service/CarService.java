package com.service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.Scanner;
import java.util.stream.Collectors;

import com.dao.CarDao;
import com.model.Brand;
import com.model.Car;

public class CarService {
	public static Scanner scanner = new Scanner(System.in);
	// private List<Car> cars;
	private CarDao carDao;

	public CarService(CarDao carDao) {
		this.carDao = carDao;
		// cars = carDao.findAll();
	}

	public void addCar() {
		String name = inputName();
		Brand brand = inputBrand();
		double price = inputPrice();
		List<Car> cars = carDao.findAll();
		int id = (cars.size() > 0) ? gernateRandomId() : 1;
		Car car = new Car(id, brand, price, name, DateFormat.formatDate(LocalDateTime.now()));
		carDao.addCar(car);
	}

	public void editCar() {
		int id = inputId();
		// scanner.nextLine();
		Optional<Car> carId = existedCar(id);
		if (carId.isPresent()) {
			Car oldCar = carId.get();
			String name = inputName();
			double price = inputPrice();
			Car newCar = new Car(oldCar.getId(), oldCar.getBrand(), price, name,
					DateFormat.formatDate(LocalDateTime.now()));
			carDao.updateCar(newCar);
			System.out.println("Cap nhat thanh cong");
		} else {
			System.out.println("Ko ton tai id xe");
		}
	}

	public void deleteCar() {
		int id = inputId();
		if (existedCar(id).isPresent()) {
			carDao.deleteCar(id);
			System.out.println("Xoa thanh cong");
		} else {
			System.out.println("Ko ton tai id xe");
		}
	}

	public void showCar() {
		carDao.findAll().stream().forEach(System.out::println);
	}

	public void findExpensiveCar() {
		// Car car = Collections.max(cars,Comparator.comparing(s -> s.getPrice()));
		carDao.findAll().stream().max(Comparator.comparing(Car::getPrice)).ifPresentOrElse(System.out::println,
				() -> System.out.println("No car"));
	}

	private Brand inputBrand() {
		StringBuilder builder = new StringBuilder("Nhap hang xe : ");
		for (Brand b : Brand.values()) {
			builder.append(b).append(",");
		}
		builder.deleteCharAt(builder.length() - 1);
		builder.append(" : ");
		String input;
		do {
			System.out.print(builder.toString());
			input = scanner.nextLine().trim().toUpperCase();
			// validate user input
			if (!Brand.isBrand(input) || !Brand.isCharater(input)) {
				System.out.println("Input ko hop le Nhap lai hang xe : ");
			}
		} while (!Brand.isBrand(input) || !Brand.isCharater(input));
		return Brand.valueOf(input);
	}

	private int gernateRandomId() {
		return carDao.findLastestId() + 1;
	}
	
	private Optional<Car> existedCar(int id) {
		Optional<Car> c = carDao.findAll().stream().filter(car -> id == car.getId()).findAny();
		return c;
	}

	private double inputPrice() {
		boolean error = true;
		double price = 0.0;
		do {
			try {
				System.out.print("Nhap gia tien ");
				price = Double.parseDouble(scanner.nextLine());
				error = false;
			} catch (NumberFormatException e) {
				System.out.println("Khong hop le !!!!");
			}
		} while (error);
		return price;
	}

	private int inputId() {
		System.out.print("Nhap id ");
		return Integer.parseInt(scanner.nextLine());
	}

	private String inputName() {
		System.out.print("Nhap ten xe: ");
		return scanner.nextLine();
	}

}
