package com.model;

import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public enum Brand {
	BMW, AUDI, TOYOTA, FERRARI, VINFAST, MERCEDES;
	
	private static final String REGEX = "[a-zA-Z]+";
	
	public static String[] getNames(Class<? extends Enum<?>> e) {
		return Arrays.stream(e.getEnumConstants()).map(Enum::name).toArray(String[]::new);
	}
	
	public static boolean isCharater(String input) {
		Pattern pattern = Pattern.compile(REGEX);
		Matcher matcher = pattern.matcher(input);
		return matcher.matches();
	}

	public static boolean isBrand(String input) {
		String[] names = getNames(Brand.class);
		return Arrays.stream(names).anyMatch(input::equals);
	}
}
