package com.model;

import java.io.Serializable;

public class Car implements Serializable{
	private int id;
	private Brand brand;
	private double price;
	private String name;
	private String createDate;

	public Car() {
	};

	public Car(int id, Brand brand, double price, String name, String createDate) {
		super();
		this.id = id;
		this.brand = brand;
		this.price = price;
		this.name = name;
		this.createDate = createDate;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Brand getBrand() {
		return brand;
	}

	public void setBrand(Brand brand) {
		this.brand = brand;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return id + " - " + name + " - " + brand + " - " + price + "$ - " + createDate;
	}
}
