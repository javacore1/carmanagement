package com.ui;

import java.util.Scanner;
import com.dao.CarDao;
import com.dao.CarIO;
import com.service.CarService;
import com.service.FileReport;
import com.service.CarReport;

public class CarMain {

	public static void main(String[] args) {
		CarDao carDao = new CarIO();
		CarService service = new CarService(carDao);
		doReport(carDao);
		while (true) {
			showMenu();
			Scanner sc = new Scanner(System.in);
			int select = Integer.parseInt(sc.nextLine());
			if (select == 1) {
				// them xe
				service.addCar();
			} else if (select == 2) {
				// sua xe
				service.editCar();
			} else if (select == 3) {
				// xoa xe
				service.deleteCar();
			} else if (select == 4) {
				// xem danh sach xe
				service.showCar();
			} else if (select == 5) {
				// Tim xe dat nhat
				service.findExpensiveCar();
			} else if (select == 6) {
				// thoat
				break;
			} else {
				System.out.println("vui long nhap lai : !");
			}
		}
	}

	private static void doReport(CarDao carDao) {
		CarReport rp = new CarReport(carDao);
		Thread t = new Thread(rp);
		t.start();
		
//		FileReport fr = new FileReport();
//		Thread t = new Thread() {
//			public void run() {
//				while (true) {
//					try {
//						Thread.sleep(1 * 60 * 1000);
//					} catch (InterruptedException e) {
//						e.printStackTrace();
//					}
//					try {
//						fr.exportFile(rp::writeContent);
//					} catch (Exception e) {
//						e.printStackTrace();
//					}
//				}
//			};
//		};
	}

	private static void showMenu() {
		System.out.println("----------Menu Car--------");
		System.out.println("1. Them xe");
		System.out.println("2. Sua xe");
		System.out.println("3. Xoa xe");
		System.out.println("4. Danh sach xe");
		System.out.println("5. Tim xe gia dat nhat");
		System.out.println("6. Thoat");
		System.out.println("Nhap lua chon : ");
	}
}
